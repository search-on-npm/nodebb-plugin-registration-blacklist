'use strict';

var winston = require.main.require('winston'),
	async = require.main.require('async'),
	db = require.main.require('./src/database'),
	meta = require.main.require('./src/meta');

var Sockets = {};

var set_lista_vecchi_username = 'connect-username-extends:lista_username_vecchi';
var blacklist_email = 'connect-registration-blacklist-email';
var blacklist_username = 'connect-registration-blacklist-username';

Sockets.isEmailInBlackList = function(socket, data, callback) {
	console.log("IN IN EMAIL " + data.email_input);
	var email_input = data.email_input;
	//console.log(email_input);
	if (!email_input) {
		return callback(new Error("Parametri non passati correttamente"));
	}
	email_input = email_input.trim();
	email_input = email_input.toLowerCase();
	meta.settings.get(blacklist_email, function(err, blacklist) {
		if (err) {
			return callback(err);
		} else {

			var email = blacklist.emailbannate;
			var variabile = 0;
			//console.log("EMAILLLLLLLL "+JSON.stringify(email));
			var lunghezza = 0;
			if (email) {
				lunghezza = email.length;
			}
			for (var i = 0; i < lunghezza; i++) {
				var regex = new RegExp(email[i]);
				if (regex.test(email_input)) {
					variabile = 1;
					break;
				}
			}
			if (variabile == 1) {
				winston.verbose('[nodebb-plugin-connect-registration-blacklist] Email bannata ' + JSON.stringify(email_input));
				return callback(null, true);
			} else {
				return callback(null, false);
			}
		}

	});

};
//Controlla che un determinato username è nella lista degli username bannati
//Deve controllare anche che un userna non sia nella lista degli usernami
//che sono stati utilizzati da altri utenti quando hanno cambiato l'username
Sockets.isUsernameInBlackList = function(socket, data, callback) {
	var username_input = data.username_input;
	if (!username_input) {
		return callback(new Error("Parametri non passati correttamente"));
	}
	username_input = username_input.trim();
	username_input = username_input.toLowerCase();

	meta.settings.get(blacklist_username, function(err, blacklist) {
		if (err) {
			return callback(err);
		} else {
			db.isSortedSetMember(set_lista_vecchi_username, username_input, function(err, isMember) {
				if (err) {
					return callback(err);
				}
				if (!isMember) {
					var username = blacklist.usernamebannate;
					var variabile = 0;
					var lunghezza = 0;
					if (username) {
						lunghezza = username.length;
					}
					for (var i = 0; i < lunghezza; i++) {
						var regex = new RegExp(username[i]);
						if (regex.test(username_input)) {
							variabile = 1;
							break;
						}
					}
					if (variabile == 1) {
						winston.verbose('[nodebb-plugin-connect-registration-blacklist] Username bannato ' + JSON.stringify(username));
						return callback(null, true);
					} else {
						return callback(null, false);
					}
				} else {
					return callback(null, {utilizzato:true});
				}

			});
		}
	});

};
//Ritorna la lista dell'email bannate
Sockets.getAllBlackListEmail = function(socket, data, callback) {
	meta.settings.get(blacklist_email, function(err, blacklist) {
		if (err) {
			return callback(err);
		} else {

			var email = blacklist.emailbannate;
			return callback(null, email);
		}
	});

};
//Ritorna la lista segli username bannati
Sockets.getAllBlackListUsername = function(socket, data, callback) {
	meta.settings.get(blacklist_username, function(err, blacklist) {
		if (err) {
			return callback(err);
		} else {
			var username = blacklist.usernamebannate;
			//console.log("LISTA USERNAME " + JSON.stringify(username));
			return callback(null, username);
		}


	});
};

//Inserisce tra l'emails bannate una nuova email
Sockets.inserisciInBlackListEmail = function(socket, data, callback) {
	var email_input = data.email_input;
	if (!email_input) {
		return callback(new Error("Parametri non passati correttamente"));
	}

	async.waterfall([
		function(next) {
			meta.settings.get(blacklist_email, next);
		},
		function(blacklist, next) {
			var email = [];
			if (blacklist && blacklist.emailbannate) {

				email = blacklist.emailbannate;
			}
			var presente = 0;
			for (var i = 0; i < email.length; i++) {
				if (email[i] == email_input) {
					presente = 1;
					break;
				}
			}
			next(null, blacklist, presente, email);

		},
		function(blacklist, presente, email, next) {
			if (presente === 0) {
				email.push(email_input);
				//email=email+email_input+" ";

				blacklist.emailbannate = email;

				//	console.log("EMAIL BANNATE ADESSO " + JSON.stringify(email));
				winston.verbose('[nodebb-plugin-connect-registration-blacklist]Email bannate ' + JSON.stringify(blacklist));
				//console.log("BLACKLIST "+JSON.stringify(blacklist));

				meta.settings.set(blacklist_email, blacklist, function(errore) {
					if (errore) {
						return callback(errore);
					} else {
						next(null, email);
					}
				});
			} else {
				return callback(null, {
					'inserimento': false,
					'numero_email_bannate': 0
				});

			}
		}
	], function(err, email) {
		if (err) {
			return callback(err);
		}
		return callback(null, {
			'inserimento': true,
			'numero_email_bannate': email.length
		});
	});

};

//Inserisce tra gli username bannati un nuovo username
Sockets.inserisciInBlackListUsername = function(socket, data, callback) {
	var username = data.username_input;
	if (!username) {
		return callback(new Error("Parametri non passati correttamente"));
	}
	async.waterfall([
		function(next) {
			meta.settings.get(blacklist_username, next);
		},
		function(blacklist, next) {
			var username_list = [];
			if (blacklist && blacklist.usernamebannate) {
				username_list = blacklist.usernamebannate;
			}
			var presente = 0;
			for (var i = 0; i < username_list.length; i++) {
				if (username_list[i] == username) {
					presente = 1;
					break;
				}
			}
			next(null, blacklist, presente, username_list);
		},

		function(blacklist, presente, username_list, next) {
			if (presente == 0) {
				username_list.push(username);
				blacklist.usernamebannate = username_list;
				//console.log("USERNAME BANNATI " + JSON.stringify(username_list));
				winston.verbose('[nodebb-plugin-connect-registration-blacklist]Username bannati ' + JSON.stringify(blacklist));
				meta.settings.set(blacklist_username, blacklist, function(errore) {
					if (errore) {
						return callback(errore);
					} else {
						next(null, username_list);
					}
				});
			} else {
				return callback(null, {
					'inserimento': false,
					'numero_username_bannati': username_list.length
				});
			}

		}
	], function(err, username_list) {
		if (err) {
			return callback(err);
		}
		return callback(null, {
			'inserimento': true,
			'numero_username_bannati': username_list.length
		});
	});
};

//Elimina un'email dalla lista dell'email bannate
Sockets.eliminaInBlackListEmail = function(socket, data, callback) {

	var email_input = data.email_input;
	if (!email_input) {
		return callback(new Error("Parametri non passati correttamente"));
	}
	async.waterfall([
		function(next) {
			meta.settings.get(blacklist_email, next);
		},
		function(blacklist, next) {
			var email = blacklist.emailbannate;
			var lunghezza = 0;
			if (email) {
				lunghezza = email.length;
			}
			var nuovo_array = [];
			for (var i = 0; i < lunghezza; i++) {
				if (email[i] != email_input) {
					nuovo_array.push(email[i]);
				}
			}
			blacklist.emailbannate = nuovo_array;
			next(null, blacklist);

		},
		function(blacklist, next) {
			meta.settings.set(blacklist_email, blacklist, function(errore) {
				if (errore) {
					return callback(errore);
				} else {
					next(null, blacklist.emailbannate);
				}
			});
		}
	], function(err, blacklist) {
		if (err) {
			return callback(err);
		}
		return callback(null, blacklist);
	});
};

//Elimina dalla lista degli username bannati un username dato
Sockets.eliminaInBlackListUsername = function(socket, data, callback) {
	var username = data.username_input;
	if (!username) {
		return callback(new Error("Parametri non passati correttamente"));
	}
	async.waterfall([
		function(next) {
			meta.settings.get(blacklist_username, next);

		},
		function(blacklist, next) {
			var username_list = blacklist.usernamebannate;
			var lunghezza = 0;

			var nuovo_array = [];
			for (var i in username_list) {
				if (username_list[i] != username) {
					nuovo_array.push(username_list[i]);
				}
			}

			//console.log("USERNAME LIST "+JSON.stringify(username_list));
			blacklist.usernamebannate = nuovo_array;
			next(null, blacklist);
		},
		function(blacklist, next) {
			meta.settings.set(blacklist_username, blacklist, function(errore) {
				if (errore) {
					return callback(errore);
				} else {
					next(null, blacklist.usernamebannate);
				}
			});
		}
	], function(err, blacklist) {
		if (err) {
			return callback(err);
		}
		return callback(null, blacklist);
	});
};


module.exports = Sockets;