"use strict";

var controllers = require('./lib/controllers'),
	meta = require.main.require('./src/meta'),
	SocketPlugin = require.main.require('./src/socket.io/plugins'),
	async = require.main.require('async'),
	sockets = require('./lib/sockets'),
	db = require.main.require('./src/database'),
	winston = require.main.require('winston'),
	plugin = {};

SocketPlugin.connectregistrationblacklist = sockets;

var blacklist_email = 'connect-registration-blacklist-email';
var blacklist_username = 'connect-registration-blacklist-username';
var set_lista_vecchi_username = 'connect-username-extends:lista_username_vecchi';

plugin.init = function(params, callback) {
	var router = params.router,
		hostMiddleware = params.middleware,
		hostControllers = params.controllers;

	// We create two routes for every view. One API call, and the actual route itself.
	// Just add the buildHeader middleware to your route and NodeBB will take care of everything for you.

	router.get('/admin/plugins/connect-registration-blacklist', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
	router.get('/api/admin/plugins/connect-registration-blacklist', controllers.renderAdminPage);

	callback();
};

//Controlla se l'email e l'username utilizzati durante la registrazione o l'update del profilo
//sono username e email validi cioè non sono bannati
plugin.dataCheck = function(data, callback) {

	//console.log("FUNZIONEEE INIZALE");
	var username;
	var email;
	if (data && data.data && data.data.username) {
		username = data.data.username;
	} else {
		//TO CHECK
		username = '';
	}
	if (data && data.data && data.data.email) {
		email = data.data.email;
	} else {
		//TO CHECK
		email = '';
	}

	username = username.trim().toLowerCase();
	email = email.trim().toLowerCase();
	async.waterfall([
		function(next) {
			db.isSortedSetMember(set_lista_vecchi_username, username, next);
		},
		function(isMember, next) {
			if (isMember) {
				return callback(new Error("Username già utilizzato"));
			}
			return next();
		},
		function(next) {
			meta.settings.get(blacklist_username, function(err, blacklist) {
				var my_err = null;
				var err_username = 0;
				//console.log(blacklist);
				var errore = 0;
				if (username && blacklist.usernamebannate && blacklist.usernamebannate != '') {
					if (err) {
						//TO CHECK
						my_err = err;
					} else {
						var blacklist_username_arr = blacklist.usernamebannate;


						for (var i in blacklist_username_arr) {
							if (new RegExp(blacklist_username_arr[i]).test(username)) {
								//my_err = new Error('Invalid Username');cavolo,passo

								errore = 1;
								err_username = 1;
								return next(null, errore, err_username);

							}


						}
						return next(null, errore, err_username);
					}
				} else {
					return next(null, errore, err_username);
				}
			});
		},
		function(errore, err_username, next) {
			meta.settings.get(blacklist_email, function(err, blacklist) {
				//console.log("EMAILLL "+JSON.stringify(blacklist));
				if (email && blacklist.emailbannate && blacklist.emailbannate != '') {
					if (err) {
						//TO CHECK
						my_err = err;
					} else {
						var blacklist_email_arr = blacklist.emailbannate;

						//	console.log("EMAILLS "+JSON.stringify(blacklist_email_arr));
						for (var i in blacklist_email_arr) {
							//		console.log("REGEX "+JSON.stringify(blacklist_email_arr[i])+" EMAIL "+JSON.stringify(email));
							if (new RegExp(blacklist_email_arr[i]).test(email)) {

								errore = 1;
								err_username = 0;
								return next(null, errore, err_username);

							}
						}
						return next(null, errore, err_username);
					}
				} else {
					return next(null, errore, err_username);
				}
			});

		}
	], function done(err, errore, err_username) {		
		if (err) {
			return callback(err);
		}

		if (errore == 0) {

			callback(null, data);
		} else {

			if (err_username == 1) {
				return callback(new Error("Username bannato"), data);
			} else {
				return callback(new Error("Email bannata"), data);
			}

		}


	});
};


plugin.addAdminNavigation = function(header, callback) {

	header.plugins.push({
		route: '/plugins/connect-registration-blacklist',
		icon: 'fa-file-image-o',
		name: 'Registration Blacklist (Connect)'
	});
	callback(null, header);
};

//Controlla che una data email è un'email bannata


module.exports = plugin;