"use strict";

(function() {
	$(document).ready(function() {
		//	console.log("DENTR");
		$('body').on('keyup', '#email', debounce(function() {
			//	$('form').on('keydown','#email',function() {
			$('#email-notify').attr('id', 'idNuovo');
			var email_inserita = $(this).val();
			email_inserita = email_inserita.trim().toLowerCase();
			if (email_inserita) {

				socket.emit('plugins.connectregistrationblacklist.isEmailInBlackList', {
					'email_input': email_inserita
				}, function(err, data) {
					if (data == true) {
						//alert(" EMAIL NOTIFY "+); //$("#email-notify").css("pointer-events", "none");
						$('#idNuovo').parent().addClass('register-danger');
						$('#idNuovo').parent().removeClass('register-success');
						//$('#email-notify').attr('id', "idNuovo");
						$('#idNuovo').text('Email Bannata');
						//	email-notify
					} else {

						$('#idNuovo').parent().removeClass('register-danger');
						$('#idNuovo').parent().addClass('register-success');
						$('#idNuovo').text('');
						$('#idNuovo').attr('id', 'email-notify');

					}
					//console.log("ELIMINA TABELLA");



				});
			}
		}, 500));

		//username
		$('body').on('keyup', '#username', debounce(function() {
			//	$('form').on('keydown','#email',function() {
			$('#username-notify').attr('id', 'idNuovoUsername');
			var username_inserito = $(this).val();
			username_inserito = username_inserito.trim().toLowerCase();
			if (username_inserito) {

				socket.emit('plugins.connectregistrationblacklist.isUsernameInBlackList', {
					'username_input': username_inserito
				}, function(err, data) {

					//console.log(data);
					//significa che è nella lista nera
					//console.log("PRIMA " + username_inserito + ": " + JSON.stringify(data));
					if (data || data.utilizzato) {

						//$("#email-notify").css("pointer-events", "none");
						if ($('#idNuovoUsernameExtends')) {
							$('#idNuovoUsernameExtends').attr('id', "idNuovoUsername");
						}
						$('#idNuovoUsername').parent().addClass('register-danger');
						$('#idNuovoUsername').parent().removeClass('register-success');
						var testo = "Username Bannato";
						if (data.utilizzato) {
							testo = "Username già utilizzato";
						}
						$('#idNuovoUsername').text(testo);

					} else {

						$('#idNuovoUsername').parent().removeClass('register-danger');
						$('#idNuovoUsername').parent().addClass('register-success');
						$('#idNuovoUsername').text('');
						$('#idNuovoUsername').attr('id', 'username-notify');

					}
					//console.log("ELIMINA TABELLA");


				});
			}
		}, 500));

	});
	//Ritarda il controllo sul testo in input per l'username e sull'email
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this,
				args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	// Note how this is shown in the console on the first load of every page
}());