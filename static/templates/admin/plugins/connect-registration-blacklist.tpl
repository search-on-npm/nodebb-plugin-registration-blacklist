<div>
    <ul class="nav nav-pills">
        <li class="active"><a href="#ban_email" data-toggle="tab">Email Bannate</a></li>
        <li><a href="#ban_username" data-toggle="tab">Username bannati </a></li>
    </ul>
    <div class="tab-content">
     <div class="tab-pane fade active in row" id="ban_email">
       <div class="post-container">
        <br />
            <br />
            <p>Per poter bannare le email basta inserire l'espressione regolare all'interno del form sottostante: (Le espressioni regolari possono essere testate <a href="https://regex101.com/">qui</a>)</p>
            <br />
            <br />
                <div style="float:left; width:100%;">
                    <form role="form">                       
                        <div class="row">
                            <div class="col-md-8 col-xs-12">
                                <div class="col-md-6 col-xs-6">
                                    <label>Inserisci un'email da bannare</label>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center">
                                    <label>Aggiungi email</label>
                                </div>
                            </div>
                        </div>
                        <br/>
                            <div class="row">
                            <div id="emailBannate">
                                <div id="inserisci_ban" class="col-md-8 col-xs-12" >
                                    <div class="col-md-6 col-xs-8">
                                        <label for="labelEmail">Email:</label>
                                         <input type="text" id="inserisci_ban_Email">
                                    </div>
                                    <div class="col-md-2 col-xs-4 text-center">
                                        <button id="confermaBanEmail" class=" bottoneBanna">
                                           +
                                        </button>
                                    </div>
                                     
                            </div>
                            </div>
                            </div>
                           
                            <br />
                            <br />
                            <!-- IF email_bannate.length -->
                            <div class="row" id="nessuna_email" hidden>
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-md-8 col-xs-12">
                                        <label ><b>Non ci sono email bannate</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row " id="nome_attributi">
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-md-6 col-xs-8">
                                        <label>Lista Email Bannate</label>
                                    </div>
                                    
                                </div>
                            </div>
                                <br/>
                                    <div id="lista_email_bannate">
                                    <!-- BEGIN email_bannate -->
                                    
                                      <div class="row " id="{email_bannate.email}">
                                        <div class="col-md-8 col-xs-12">
                                            <div class="col-md-6 col-xs-8">{email_bannate.email}</div>
                                            <div class="col-md-2 col-xs-4 text-center">
                                                <button  id="elimina_Ban_email{email_bannate.email}" class="bottoneDelete" title="Vuoi eliminare l'email bannata: {email_bannate.email}?">
                                                   X
                                                </button>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row" id="linea_{email_bannate.email}">
                                        <div class="col-md-5  col-xs-11">
                                            
                                                <hr/>

                                            
                                        </div>
                                        <br/>
                                    </div>
                                    
                                    
                                    <!-- END email_bannate -->
                                </div>
                            
                            
                         
                            <!-- ELSE -->
                            <div class="row" >
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-md-8 col-xs-12">
                                        <label id="nessuna_email"><b>Non ci sono email bannate</b></label>
                                    </div>
                                </div>
                            </div>
                            <div id="lista_email_bannate"></div>
                            
                            <!-- ENDIF email_bannate.length -->
                             </form>
                        </div>
                        
                </div>
            </div>
      

        <div class="tab-pane fade in row" id="ban_username">
            <div class="post-container" >
                <br />
                <br />
                <p>Per poter bannare gli username basta inserire l'espressione regolare all'interno del form sottostante: (Le espressioni regolari possono essere testate <a href="https://regex101.com/">qui</a>)</p>
                <br />
                <br />
                <div style="float:left; width:100%;">
                <form role="form" > 
                    <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-md-6 col-xs-8">
                                        <label>Inserisci un username da bannare</label>
                                    </div>
                                <div class="col-md-3 col-xs-4">
                                    <label>Aggiungi username</label>
                                </div>
                            </div>
                        </div>
                        <br/>                       
                        <div class="row" id="inserisci_ban_username" >
                            <div class="col-md-8 col-xs-12">
                                <div class="col-md-6 col-xs-8">
                                    <label for="labelUsername">Username:</label>                                
                                    <input type="text" id="bannaUsername">
                                </div>
                                <div class="col-md-2 col-xs-4 text-center">
                                    <button id="confermaBanUsername" class="bottoneBanna">
                                    +
                                    </button>
                                </div>

                            </div>
                        </div>
                        <br />
                        <br />
                        <!-- IF username_bannati.length -->
                        <div class="row" id="nessun_username" hidden>
                                    <div class="col-md-8 col-xs-12">
                                        <div class="col-md-8 col-xs-12">
                                            <b><label>Non ci sono username bannati</label></b>
                                        </div>
                                    </div>
                                </div>                            
                            <div class="row " id="nome_attributi_username">
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-md-6 col-xs-8">
                                        <label>Lista Username Bannati</label>
                                    </div>
                                    
                                    </div>
                            </div>
                            <br/>
                                    <div id="lista_username_bannati">
                                          
                              
                                    <!-- BEGIN username_bannati -->
                                    <div class="row" id="{username_bannati.username}">
                                        <div class="col-md-8 col-xs-12">
                                            <div class="col-md-6 col-xs-8">{username_bannati.username}
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                              <button  title="Vuoi eliminare l'username bannata: {username_bannati.username}" id="elimina_Ban_username{username_bannati.username}" class="bottoneDelete">
                                               X
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row" id="linea_{username_bannati.username}">
                                        <div class="col-md-5 col-xs-11">
                                            
                                                <hr>

                                            
                                        </div>
                                        <br/>
                                    </div>
                                    <!-- END username_bannati -->
                                    </div>
                            <!-- ELSE -->
                              <div class="row" id="nessun_username">
                                    <div class="col-md-8 col-xs-12">
                                        <div class="col-md-8 col-xs-12">
                                            <b><label>Non ci sono username bannati</label></b>
                                        </div>
                                    </div>
                                </div>
                                <div id="lista_username_bannati"></div>

                            
                            <!-- ENDIF username_bannati.length -->

                            
                   
                </form>
                </div>

            </div>

        </div>
       
       
    </div>
   
</div>