'use strict';
var async = require.main.require('async'),
	winston = require.main.require('winston');
var Controllers = {};
var meta = require.main.require('./src/meta');
var blacklist_email = 'connect-registration-blacklist-email';
var blacklist_username = 'connect-registration-blacklist-username';

Controllers.renderAdminPage = function(req, res, callback) {
	async.waterfall([
		function(next) {
			meta.settings.get(blacklist_email, function(err, blacklist) {
				if (err) {
					return callback(err);
				} else {

					var email = blacklist.emailbannate;
					next(null, email);
				}
			});
		},
		function(email, next) {
			meta.settings.get(blacklist_username, function(err, blacklist) {
				if (err) {
					return callback(err);
				} else {
					var username = blacklist.usernamebannate;
					//console.log("LISTA USERNAME " + JSON.stringify(username));
					next(null, email, username);
				}


			});
		}
	], function done(errore, email, username) {
		if (errore) {
			return callback(errore);
		}
		var email_bannate = [];
		var username_bannati = [];
		if (email != null && email != undefined && email.length != 0) {
			for (var i = 0; i < email.length; i++) {
				email_bannate.push({
					'email': email[i]
				});
			}
		}
		if (username != null && username != undefined && username.length != 0) {
			for (var i = 0; i < username.length; i++) {
				username_bannati.push({
					'username': username[i]
				});
			}
		}


		winston.verbose("[connect-registration-blacklist] EMAIL BANNATE " + JSON.stringify(email) + " USERNAME BANNATE " + JSON.stringify(username));
		res.render('admin/plugins/connect-registration-blacklist', {
			'email_bannate': email_bannate,
			'username_bannati': username_bannati
		});
	});
};
//Controlla se l'email passata in input è già presente


module.exports = Controllers;
