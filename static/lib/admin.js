define('admin/plugins/connect-registration-blacklist', ['settings'], function(Settings) {
	'use strict';
	/* globals $, app, socket, require */

	var ACP = {};
	var frase;
	var frase_username;
	ACP.init = function() {
		Settings.load('connect-registration-blacklist', $('.connect-registration-blacklist-settings'));

		$('#save').on('click', function() {
			Settings.save('connect-registration-blacklist', $('.connect-registration-blacklist-settings'), function() {
				app.alert({
					type: 'success',
					alert_id: 'connect-registration-blacklist-saved',
					title: 'Settings Saved',

				});
			});
		});
		$('#confermaBanEmail').on('click', function(event) {
			event.preventDefault();
			var email = $('#inserisci_ban_Email').val();
			email = email.trim().toLowerCase();
			if (email) {
				var lista_email_bannate = $('#lista_email_bannate');
				var regex;
				try {
					regex = new RegExp(email);
					socket.emit('plugins.connectregistrationblacklist.inserisciInBlackListEmail', {
						'email_input': email
					}, function(err, data) {
						if (data != null && data.inserimento == true) {

							if (data.numero_email_bannate == 1) {
								lista_email_bannate.removeAttr('hidden');
								$('#nessuna_email').prop('hidden', true);
								$('#nome_attributi').removeAttr('hidden');
							}
							console.log('<div class="row" id="' + email + '"><div class="col-md-8 col-xs-12"><div class="col-md-6 col-xs-8">' + email + '</div><div class="col-md-2 col-xs-4 text-center"> <button  id="elimina_Ban_email' + email + '" class="bottoneDelete" title="Vuoi eliminare l\'email bannata: ' + email + '?">X</button></div></div></div><div class="row" id="linea_' + email + '"><div class="col-md-5 col-xs-11"><hr/></div><br/></div>');
							lista_email_bannate.append('<div class="row" id="' + email + '"><div class="col-md-8 col-xs-12"><div class="col-md-6 col-xs-8">' + email + '</div><div class="col-md-2 col-xs-4 text-center"> <button  id="elimina_Ban_email' + email + '" class="bottoneDelete" title="Vuoi eliminare l\'email bannata: ' + email + '?">X</button></div></div></div><div class="row" id="linea_' + email + '"><div class="col-md-5 col-xs-11"><hr/></div><br/></div>');
						} else {
							app.alertError("L'email è stata già bannata");
						}
						//console.log("Confermaaa "+JSON.stringify(conferma_ban));
					});
				} catch (e) {
					app.alertError("Inserire correttamente l'espressione regolare");
				}

			} else {
				app.alert({
					type: 'danger',
					alert_id: 'connect-registration-blacklist-saved-email',
					title: 'Email Input Error',
					message: 'Parametri inseriti errati',
					clickfn: function() {
						socket.emit('admin.reload');
					}
				});
			}

		});


		$('body').on('click', '[id^=elimina_Ban_email]', function(event) {
			event.preventDefault();
			var email_da_eliminare = $(this).attr('id').replace('elimina_Ban_email', '');


			//$.ajax({
			//	url:'/api/admin/plugins/connect-registration-blacklist/eliminaInBlackListEmail',
			socket.emit('plugins.connectregistrationblacklist.eliminaInBlackListEmail', {
				'email_input': email_da_eliminare
			}, function(err, data) {

				//tabella.append(email+"\n");
				var lista_email_bannate = $('#lista_email_bannate');
				lista_email_bannate.find('div[id="' + email_da_eliminare + '"]').remove();
				lista_email_bannate.find('div[id="linea_' + email_da_eliminare + '"]').remove();
				if (data.length == 0) {
					$('#nome_attributi').attr('hidden', true);
					$('#nessuna_email').attr('hidden', false);
					//tabella.attr('hidden', true);
				}


			});

		});


		$('#confermaBanUsername').on('click', function(event) {
			event.preventDefault();
			var lista_username_bannati = $('#lista_username_bannati');
			var conferma_ban = $('#bannaUsername').val();
			conferma_ban = conferma_ban.trim().toLowerCase();
			if (conferma_ban) {
				try {
					var regex = new RegExp(conferma_ban);
					socket.emit('plugins.connectregistrationblacklist.inserisciInBlackListUsername', {
						'username_input': conferma_ban
					}, function(err, data) {

						if (data != null && data.inserimento == true) {
							if (data.numero_username_bannati == 1) {
								lista_username_bannati.removeAttr('hidden');
								$('#nessun_username').prop('hidden', true);
								$('#nome_attributi_username').prop('hidden', false);
							}
							lista_username_bannati.append('<div class="row" id="' + conferma_ban + '"><div class="col-md-8 col-xs-12"> <div class="col-md-6 col-xs-8">' + conferma_ban + '</div> <div class="col-md-2 col-xs-4"> <button  id="elimina_Ban_username' + conferma_ban + '" class="bottoneDelete" title="Vuoi eliminare l\'username bannata: ' + conferma_ban + '">X</button></div> </div></div> <div class="row" id="linea_' + conferma_ban + '"><div class="col-md-5 col-xs-11"> <hr/></div><br/></div>');
						} else {
							app.alertError("Username già inserito");
							//console.log("Confermaaa "+JSON.stringify(conferma_ban));
						}

					});
				} catch (e) {
					app.alertError("Inserire correttamente l'espressione regolare");

				}


			} else {
				app.alert({
					type: 'danger',
					alert_id: 'connect-registration-blacklist-saved-username',
					title: 'Username Input Error',
					message: 'Parametri inseriti errati',
					clickfn: function() {
						socket.emit('admin.reload');
					}

				});
			}

		});
		$('body').on('click', '[id^="elimina_Ban_username"]', function(event) {
			event.preventDefault();
			var username_da_bannare = $(this).attr('id').replace('elimina_Ban_username', '');
			var lista_username_bannati = $('#lista_username_bannati');
			socket.emit('plugins.connectregistrationblacklist.eliminaInBlackListUsername', {
				'username_input': username_da_bannare
			}, function(err, data) {
				lista_username_bannati.find('div[id="' + username_da_bannare + '"]').remove();
				lista_username_bannati.find('div[id="linea_' + username_da_bannare + '"]').remove();
				if (data.length == 0) {
					$('#nessun_username').prop('hidden', false);
					$('#nome_attributi_username').prop('hidden', true);
					lista_username_bannati.prop('hidden', true);
				}


			});

		});


	};


	return ACP;



});