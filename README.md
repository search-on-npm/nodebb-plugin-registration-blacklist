# Registration-Blacklist Plugin for Connect

**[FUNZIONALITA']** <br />

Il plugin permette, lato amministrazione, di inserire  gli username e le emails che non potranno essere usate dagli utenti per registrarsi al forum. Per proibire l'iscrizione degli utenti utilizzando certi username e email è possibile utilizzare le espressioni regolari. Di seguito viene mostrato come comparirà la pagina, lato admin, del plugin:

<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-registration-blacklist/raw/demaoui/screenshot/connect-registration-blacklist-admin-menu.png)

<br />
Dal menu in alto a sinistra è possibile passare dal bannare email al bannare username. Per esempio per poter bannare email basta inserire nel campo "Email" un'espressione regolare (in questo caso verranno bannate le email che iniziano per "prova"): <br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-registration-blacklist/raw/demaoui/screenshot/connect-registration-blacklist-email.png)


<br />
Nello stesso modo è possibile bannare anche gli username. Una volta inserita quindi la regex che permette di bloccare l'iscrizione di alcune email, quanto un utente proverà a registrarsi con quella email comparirà:
<br />
![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-registration-blacklist/raw/demaoui/screenshot/connect-registration-blacklist-block.png)

<br />
